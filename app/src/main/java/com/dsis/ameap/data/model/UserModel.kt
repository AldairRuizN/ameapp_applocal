package com.dsis.ameap.data.model

import com.google.gson.annotations.SerializedName

data class UserModel(
    @SerializedName("id") val quote: String,
    @SerializedName("name") val author: String
)

