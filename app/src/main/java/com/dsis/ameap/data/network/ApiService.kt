package com.dsis.ameap.data.network

import com.dsis.ameap.responses.CountResponse
import com.dsis.ameap.responses.MateriaResponse
import com.dsis.ameap.responses.UserLResponse
import com.dsis.ameap.responses.UserStyle
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @Headers("Content-Type: application/json")
    @POST("users/validar")
    fun getUserValidate(@Body userLResponse: UserLResponse):Call<UserLResponse>

    @GET("estilosUsuario/count/{id}")
    fun getCountEstilosUser(@Path("id") id:Int):Call<CountResponse>

    @GET("estilosUsuario/{id}")
    fun getEstilossUser(@Path("id") id:Int):Call<List<UserStyle>>

    @Headers("Content-Type: application/json")
    @POST("estilosUsuario/")
    fun postUserStyle(@Body userStyle: UserStyle):Call<UserStyle>

    @GET("materias")
    fun getMaterias():Call<List<MateriaResponse>>
}