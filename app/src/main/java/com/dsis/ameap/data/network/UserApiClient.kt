package com.dsis.ameap.data.network

import com.dsis.ameap.data.model.UserModel
import retrofit2.Response
import retrofit2.http.GET

interface UserApiClient {
    @GET("/.json")
    suspend fun getAllUsers(): Response<List<UserModel>>
}