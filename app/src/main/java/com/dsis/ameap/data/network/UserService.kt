package com.dsis.ameap.data.network

import com.dsis.ameap.core.RetrofitHelper
import com.dsis.ameap.data.model.UserModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserService {
    private val retrofit = RetrofitHelper.getRetrofit()
    suspend fun getUsers(): List<UserModel> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(UserApiClient::class.java).getAllUsers()
            response.body() ?: emptyList()
        }
    }
}