package com.dsis.ameap.repository

import com.dsis.ameap.core.ServiceBuilder
import com.dsis.ameap.data.network.ApiService
import com.dsis.ameap.responses.CountResponse
import com.dsis.ameap.responses.MateriaResponse
import com.dsis.ameap.responses.UserLResponse
import com.dsis.ameap.responses.UserStyle
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiRepository {
    fun getUserValidate(userL: UserLResponse, onResult: (UserLResponse?) -> Unit){
        val retrofit = ServiceBuilder.buildService(ApiService::class.java)
        retrofit.getUserValidate(userL).enqueue(
            object : Callback<UserLResponse>{
                override fun onFailure(call: Call<UserLResponse>, t: Throwable) {
                    onResult(null)
                }
                override fun onResponse( call: Call<UserLResponse>, response: Response<UserLResponse>) {
                    val addedUser = response.body()
                    onResult(addedUser)
                }
            }
        )
    }

    fun postUserStyle(userStyle: UserStyle, onResult: (UserStyle?) -> Unit){
        val retrofit = ServiceBuilder.buildService(ApiService::class.java)
        retrofit.postUserStyle(userStyle).enqueue(
            object : Callback<UserStyle>{
                override fun onFailure(call: Call<UserStyle>, t: Throwable) {
                    onResult(null)
                }
                override fun onResponse( call: Call<UserStyle>, response: Response<UserStyle>) {
                    val addedUserStyle = response.body()
                    onResult(addedUserStyle)
                }
            }
        )
    }

    fun getMaterias(onResult: (List<MateriaResponse>?) -> Unit){
        val retrofit = ServiceBuilder.buildService(ApiService::class.java)
        retrofit.getMaterias().enqueue(
            object : Callback<List<MateriaResponse>>{
                override fun onFailure(call: Call<List<MateriaResponse>>, t: Throwable) {
                    onResult(null)
                }
                override fun onResponse( call: Call<List<MateriaResponse>>, response: Response<List<MateriaResponse>>) {
                    val materias = response.body()
                    onResult(materias)
                }
            }
        )
    }

    fun getCountEstilosUser(id:Int, onResult: (CountResponse?) -> Unit){
        val retrofit = ServiceBuilder.buildService(ApiService::class.java)
        retrofit.getCountEstilosUser(id).enqueue(
            object : Callback<CountResponse>{
                override fun onFailure(call: Call<CountResponse>, t: Throwable) {
                    onResult(null)
                }
                override fun onResponse( call: Call<CountResponse>, response: Response<CountResponse>) {
                    val userStylesCount = response.body()
                    onResult(userStylesCount)
                }
            }
        )
    }

    fun getEstilossUser(id: Int, onResult: (List<UserStyle>?) -> Unit){
        val retrofit = ServiceBuilder.buildService(ApiService::class.java)
        retrofit.getEstilossUser(id).enqueue(
            object : Callback<List<UserStyle>>{
                override fun onFailure(call: Call<List<UserStyle>>, t: Throwable) {
                    onResult(null)
                }
                override fun onResponse( call: Call<List<UserStyle>>, response: Response<List<UserStyle>>) {
                    val userStyles = response.body()
                    onResult(userStyles)
                }
            }
        )
    }

}