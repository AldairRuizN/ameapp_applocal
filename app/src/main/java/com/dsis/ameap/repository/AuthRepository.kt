package com.dsis.ameap.repository

import com.dsis.ameap.data.network.AuthApi

class AuthRepository (
        private val api: AuthApi
        ) : BaseRepository(){
            suspend fun login(
                email: String,
                password: String
            ) = safeApiCall {
                api.login(email,password)
            }
        }