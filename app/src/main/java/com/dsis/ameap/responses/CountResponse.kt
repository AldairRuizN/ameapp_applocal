package com.dsis.ameap.responses

import com.google.gson.annotations.SerializedName

data class CountResponse(
    @SerializedName("count") val count:Int
)
