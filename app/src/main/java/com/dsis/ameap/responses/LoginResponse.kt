package com.dsis.ameap.responses

data class LoginResponse(
    val access_token: String,
    val expires_in: Int,
    val status: Int,
    val success: String,
    val token_type: String,
    val user: User
)