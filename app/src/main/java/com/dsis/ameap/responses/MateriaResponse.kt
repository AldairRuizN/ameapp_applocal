package com.dsis.ameap.responses

import com.google.gson.annotations.SerializedName

data class MateriaResponse (
    @SerializedName("id") val id:Int,
    @SerializedName("nombre") val nombre:String,
    @SerializedName("grado_id") val grado_id:Int,
    @SerializedName("created_at") val created_at:String,
    @SerializedName("updated_at") val updated_at:String
)