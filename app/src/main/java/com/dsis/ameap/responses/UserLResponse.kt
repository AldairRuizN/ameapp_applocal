package com.dsis.ameap.responses

import com.google.gson.annotations.SerializedName

data class UserLResponse(
    @SerializedName("id") val id:Int,
    @SerializedName("name") val name:String,
    @SerializedName("email") val email:String,
    @SerializedName("password") val password:String,
    @SerializedName("status") val status:Boolean,
    @SerializedName("mensaje") val mensaje:String,

    )
