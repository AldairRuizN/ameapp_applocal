package com.dsis.ameap.responses

import com.google.gson.annotations.SerializedName

data class UserStyle(
    @SerializedName("id") val id:Int,
    @SerializedName("prioridad") val prioridad:String,
    @SerializedName("usuario_id") val usuario_id:Int,
    @SerializedName("estilo_id") val estilo_id:Int,
    @SerializedName("created_at") val created_at:String,
    @SerializedName("updated_at") val updated_at:String,
)
