package com.dsis.ameap.ui.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.android.gms.location.ActivityTransitionResult
import java.text.SimpleDateFormat


class ActivityTransitionReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (ActivityTransitionResult.hasResult(intent)) {
                val result = ActivityTransitionResult.extractResult(intent)
                result?.let {
                    result.transitionEvents.forEach { event ->
                        //Info for debugging purposes
                        val info =
                            "Transition: " + ActivityTransitionUtil.toActivityString(event.activityType) +
                                    " (" + ActivityTransitionUtil.toTransitionType(event.transitionType) + ")"




                        Toast.makeText(context, info, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }
