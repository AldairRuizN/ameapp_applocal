package com.dsis.ameap.ui.view

object Constant {

    fun getQuestions(): ArrayList<Questions> {

        val questionsList = ArrayList<Questions>()


        val que1 = Questions(
            1,
            "1. Tengo fama de decir lo que pienso claramente y sin rodeos.",
            "si",
            "No",
            1
        )
        questionsList.add(que1)
        val que2 = Questions(
            2,
            "2. Estoy seguro(a) de lo que es bueno y lo que es malo, lo que está bien y lo que está mal.",
            "si",
            "No",
            1
        )
        questionsList.add(que2)
        val que3 = Questions(3, "3. Muchas veces actúo sin mirar las consecuencias.", "si", "No", 1)
        questionsList.add(que3)
        val que4 = Questions(
            4,
            "4. Normalmente trato de resolver los problemas metódicamente y paso a paso.",
            "si",
            "No",
            1
        )

        val que5 = Questions(
            5,
            "5. Creo que los formalismos coartan y limitan la actuación libre de las personas.",
            "si",
            "No",
            1
        )

        val que6 = Questions(
            6,
            "6. Me interesa saber cuáles son los sistemas de valores de los demás y con qué criterios actúan.",
            "si",
            "No",
            1
        )

        val que7 = Questions(
            7,
            "7. Pienso que el actuar intuitivamente puede ser siempre tan válido como actuar reflexivamente.",
            "si",
            "No",
            1
        )

        val que8 =
            Questions(8, "8. Creo que lo más importante es que las cosas funcionen.", "si", "No", 1)

        val que9 =
            Questions(9, "9. Procuro estar al tanto de lo que ocurre aquí y ahora.", "si", "No", 1)

        val que10 = Questions(
            10,
            "10. Disfruto cuando tengo tiempo para preparar mi trabajo y realizarlo a conciencia.",
            "si",
            "No",
            1
        )

        val que11 = Questions(
            11,
            "11. Estoy a gusto siguiendo un orden, en las comidas, en el estudio, haciendo ejercicio regularmente.",
            "si",
            "No",
            1
        )

        val que12 = Questions(
            12,
            "12. Cuando escucho una nueva idea enseguida comienzo a pensar como ponerla en práctica.",
            "si",
            "No",
            1
        )

        val que13 = Questions(
            13,
            "13. Prefiero las ideas originales y novedosas aunque no sean prácticas.",
            "si",
            "No",
            1
        )

        val que14 = Questions(
            14,
            "14. Admito y me ajusto a las normas sólo si me sirven para lograr mis objetivos.",
            "si",
            "No",
            1
        )

        val que15 = Questions(
            15,
            "15. Normalmente encajo bien con personas reflexivas, y me cuesta sintonizar con personas demasiado espontáneas, imprevisibles.",
            "si",
            "No",
            1
        )

        val que16 = Questions(16, "16. Escucho con más frecuencia que lo que hablo.", "si", "No", 1)

        val que17 =
            Questions(17, "17. Prefiero las cosas estructuradas a las desordenadas.", "si", "No", 1)

        val que18 = Questions(
            18,
            "18. Cuando poseo cualquier información, trato de interpretarla bien antes de manifestar alguna conclusión.",
            "si",
            "No",
            1
        )

        val que19 = Questions(
            19,
            "19. Antes de hacer algo estudio con cuidado sus ventajas e inconvenientes.",
            "si",
            "No",
            1
        )
        val que20 = Questions(
            20,
            "20. Me crezco con el reto de hacer algo nuevo y diferente.",
            "si",
            "No",
            1
        )
        val que21 = Questions(
            21,
            "21. Casi siempre procuro ser coherente con mis criterios y sistemas de valores. Tengo principios y los sigo.",
            "si",
            "No",
            1
        )
        val que22 =
            Questions(22, "22. Cuando hay una discusión no me gusta ir con rodeos.", "si", "No", 1)
        val que23 = Questions(
            23,
            "23. Me disgusta implicarme afectivamente en mi ambiente de trabajo. Prefiero mantener relaciones distantes.",
            "si",
            "No",
            1
        )
        val que24 = Questions(
            24,
            "24. Me gustan más las personas realistas y concretas que las teóricas.",
            "si",
            "No",
            1
        )
        val que25 =
            Questions(25, "25. Me cuesta ser creativo(a), romper estructuras.", "si", "No", 1)
        val que26 = Questions(
            26,
            "26. Me siento a gusto con personas espontaneas y divertidas.",
            "si",
            "No",
            1
        )
        val que27 = Questions(
            27,
            "27. La mayoría de las veces expreso abiertamente cómo me siento.",
            "si",
            "No",
            1
        )
        val que28 = Questions(28, "28. Me gusta analizar y dar vueltas a las cosas.", "si", "No", 1)
        val que29 = Questions(
            29,
            "29. Me molesta que la gente no se tome en serio las cosas.",
            "si",
            "No",
            1
        )
        val que30 = Questions(
            30,
            "30. Me atrae experimentar y practicar las últimas técnicas y novedades.",
            "si",
            "No",
            1
        )
        val que31 =
            Questions(31, "31. Soy cauteloso(a) a la hora de sacar conclusiones.", "si", "No", 1)
        val que32 = Questions(
            32,
            "32. Prefiero contar con el mayor número de fuentes de información. Cuantos más datos reúna para reflexionar, mejor.",
            "si",
            "No",
            1
        )
        val que33 = Questions(33, "33. Tiendo a ser perfeccionista.", "si", "No", 1)
        val que34 = Questions(
            34,
            "34. Prefiero oír las opiniones de los demás antes de exponer la mía.",
            "si",
            "No",
            1
        )
        val que35 = Questions(
            35,
            "35. Me gusta afrontar la vida espontáneamente y no tener que planificar todo previamente.",
            "si",
            "No",
            1
        )
        val que36 = Questions(
            36,
            "36. En las discusiones me gusta observar cómo actúan los demás participantes.",
            "si",
            "No",
            1
        )
        val que37 = Questions(
            37,
            "37. Me siento incómodo(a) con las personas calladas y demasiado analíticas.",
            "si",
            "No",
            1
        )
        val que38 = Questions(
            38,
            "38. Juzgo con frecuencia las ideas de los demás por su valor práctico.",
            "si",
            "No",
            1
        )
        val que39 = Questions(
            39,
            "39. Me agobio si me obligan a acelerar mucho el trabajo para cumplir un plazo.",
            "si",
            "No",
            1
        )
        val que40 = Questions(
            40,
            "40. En las reuniones apoyo las ideas prácticas y realistas.",
            "si",
            "No",
            1
        )
        val que41 = Questions(
            41,
            "41. Es mejor gozar del momento presente que deleitarse pensando en el paso o en el futuro.",
            "si",
            "No",
            1
        )
        val que42 = Questions(
            42,
            "42. Me molestan las personas que siempre desean apresurar las cosas.",
            "si",
            "No",
            1
        )
        val que43 = Questions(
            43,
            "43. Aporto ideas nuevas y espontáneas en los grupos de discusión.",
            "si",
            "No",
            1
        )
        val que44 = Questions(
            44,
            "44. Pienso que son más consistentes las decisiones fundamentadas en un minucioso análisis que las basadas en la intuición.",
            "si",
            "No",
            1
        )
        val que45 = Questions(
            45,
            "45. Detecto frecuentemente la inconsistencia y puntos débiles en las argumentaciones de los demás.",
            "si",
            "No",
            1
        )
        val que46 = Questions(
            46,
            "46. Creo que es preciso saltarse las normas muchas más veces que cumplirlas.",
            "si",
            "No",
            1
        )
        val que47 = Questions(
            47,
            "47. A menudo caigo en la cuenta de otras formas mejores y más prácticas de hacer las cosas.",
            "si",
            "No",
            1
        )
        val que48 = Questions(48, "48. En conjunto hablo más que escucho.", "si", "No", 1)
        val que49 = Questions(
            49,
            "49. Prefiero distanciarme de los hechos y observarlos desde otras perspectivas.",
            "si",
            "No",
            1
        )
        val que50 = Questions(
            50,
            "50. Estoy convencido(a) que debe imponerse la lógica y el razonamiento.",
            "si",
            "No",
            1
        )
        val que51 = Questions(51, "51. Me gusta buscar nuevas experiencias.", "si", "No", 1)
        val que52 = Questions(52, "52. Me gusta experimentar y aplicar las cosas.", "si", "No", 1)
        val que53 = Questions(
            53,
            "53. Pienso que debemos llegar pronto al grano, al meollo de los temas.",
            "si",
            "No",
            1
        )
        val que54 = Questions(
            54,
            "54. Siempre trato de conseguir conclusiones e ideas claras.",
            "si",
            "No",
            1
        )
        val que55 = Questions(
            55,
            "55. Prefiero discutir cuestiones concretas y no perder el tiempo con charlas vacías.",
            "si",
            "No",
            1
        )
        val que56 = Questions(
            56,
            "56. Me impaciento cuando me dan explicaciones irrelevantes e incoherentes.",
            "si",
            "No",
            1
        )
        val que57 =
            Questions(57, "57. Compruebo antes si las cosas funcionan realmente.", "si", "No", 1)
        val que58 = Questions(
            58,
            "58. Hago varios borradores antes de la redacción definitiva de un trabajo.",
            "si",
            "No",
            1
        )
        val que59 = Questions(
            59,
            "59. Soy consciente de que en las discusiones ayudo a mantener a los demás centrados en el tema, evitando divagaciones.",
            "si",
            "No",
            1
        )
        val que60 = Questions(
            60,
            "60. Observo que, con frecuencia, soy uno(a) de los(as) más objetivos(as) y desapasionados(as) en las discusiones.",
            "si",
            "No",
            1
        )
        val que61 = Questions(
            61,
            "61. Cuando algo va mal, le quito importancia y trato de hacerlo mejor.",
            "si",
            "No",
            1
        )
        val que62 = Questions(
            62,
            "62. Rechazo ideas originales y espontáneas si no las veo prácticas.",
            "si",
            "No",
            1
        )
        val que63 = Questions(
            63,
            "63. Me gusta sopesar diversas alternativas antes de tomar una decisión.",
            "si",
            "No",
            1
        )
        val que64 = Questions(
            64,
            "64. Con frecuencia miro hacia adelante para prever el futuro.",
            "si",
            "No",
            1
        )
        val que65 = Questions(
            65,
            "65. En los debates y discusiones prefiero desempeñar un papel secundario antes que ser el(la) líder o el(la) que más participa.",
            "si",
            "No",
            1
        )
        val que66 =
            Questions(66, "66. Me molestan las personas que no actúan con lógica.", "si", "No", 1)
        val que67 = Questions(
            67,
            "67. Me resulta incómodo tener que planificar y prever las cosas.",
            "si",
            "No",
            1
        )
        val que68 = Questions(
            68,
            "68. Creo que el fin justifica los medios en muchos casos.",
            "si",
            "No",
            1
        )
        val que69 =
            Questions(69, "69. Suelo reflexionar sobre los asuntos y problemas.", "si", "No", 1)
        val que70 = Questions(
            70,
            "70. El trabajar a conciencia me llena de satisfacción y orgullo.",
            "si",
            "No",
            1
        )
        val que71 = Questions(
            71,
            "71. Ante los acontecimientos trato de descubrir los principios y teorías en que se basa.",
            "si",
            "No",
            1
        )
        val que72 = Questions(
            72,
            "72. Con tal de conseguir el objetivo que pretendo soy capaz de herir sentimientos ajenos.",
            "si",
            "No",
            1
        )
        val que73 = Questions(
            73,
            "73. No me importa hacer todo lo necesario para que sea efectivo mi trabajo.",
            "si",
            "No",
            1
        )
        val que74 = Questions(
            74,
            "74. Con frecuencia soy una de las personas más anima las fiestas.",
            "si",
            "No",
            1
        )
        val que75 = Questions(
            75,
            "75. Me aburro enseguida con el trabajo metódico y minucioso.",
            "si",
            "No",
            1
        )
        val que76 = Questions(
            76,
            "76. La gente con frecuencia cree que soy poco sensible a sus sentimientos.",
            "si",
            "No",
            1
        )
        val que77 = Questions(77, "77. Suelo dejarme llevar por mis intuiciones.", "si", "No", 1)
        val que78 = Questions(
            78,
            "78. Si trabajo en grupo procuro que se siga un método y un orden.",
            "si",
            "No",
            1
        )
        val que79 = Questions(
            79,
            "79. Con frecuencia me interesa averiguar lo que piensa la gente.",
            "si",
            "No",
            1
        )
        val que80 = Questions(
            80,
            "80. Esquivo los temas subjetivos, ambiguos y poco claros.",
            "si",
            "No",
            1
        )


        questionsList.add(que4)
        questionsList.add(que5)
        questionsList.add(que6)
        questionsList.add(que7)
        questionsList.add(que8)
        questionsList.add(que9)
        questionsList.add(que10)
        questionsList.add(que11)
        questionsList.add(que12)
        questionsList.add(que13)
        questionsList.add(que14)
        questionsList.add(que15)
        questionsList.add(que16)
        questionsList.add(que17)
        questionsList.add(que18)
        questionsList.add(que19)
        questionsList.add(que20)
        questionsList.add(que21)
        questionsList.add(que22)
        questionsList.add(que23)
        questionsList.add(que24)
        questionsList.add(que25)
        questionsList.add(que26)
        questionsList.add(que27)
        questionsList.add(que28)
        questionsList.add(que29)
        questionsList.add(que30)
        questionsList.add(que31)
        questionsList.add(que32)
        questionsList.add(que33)
        questionsList.add(que34)
        questionsList.add(que35)
        questionsList.add(que36)
        questionsList.add(que37)
        questionsList.add(que38)
        questionsList.add(que39)
        questionsList.add(que40)
        questionsList.add(que41)
        questionsList.add(que42)
        questionsList.add(que43)
        questionsList.add(que44)
        questionsList.add(que45)
        questionsList.add(que46)
        questionsList.add(que47)
        questionsList.add(que48)
        questionsList.add(que49)
        questionsList.add(que50)
        questionsList.add(que51)
        questionsList.add(que52)
        questionsList.add(que53)
        questionsList.add(que54)
        questionsList.add(que55)
        questionsList.add(que56)
        questionsList.add(que57)
        questionsList.add(que58)
        questionsList.add(que59)
        questionsList.add(que60)
        questionsList.add(que61)
        questionsList.add(que62)
        questionsList.add(que63)
        questionsList.add(que64)
        questionsList.add(que65)
        questionsList.add(que66)
        questionsList.add(que67)
        questionsList.add(que68)
        questionsList.add(que69)
        questionsList.add(que70)
        questionsList.add(que71)
        questionsList.add(que72)
        questionsList.add(que73)
        questionsList.add(que74)
        questionsList.add(que75)
        questionsList.add(que76)
        questionsList.add(que77)
        questionsList.add(que78)
        questionsList.add(que79)
        questionsList.add(que80)

        return questionsList
    }

}