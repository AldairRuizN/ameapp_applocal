package com.dsis.ameap.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import com.dsis.ameap.R
import com.dsis.ameap.databinding.ActivityContenidosBinding
import com.dsis.ameap.databinding.ActivityCuestionarioBinding
import com.dsis.ameap.repository.ApiRepository
import com.dsis.ameap.responses.MateriaResponse
import com.dsis.ameap.responses.UserStyle
import com.dsis.ameap.ui.viewmodel.Session.Companion.prefs

class Contenidos : AppCompatActivity() {

    lateinit var binding: ActivityContenidosBinding
    var listStylesU :  List<UserStyle>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContenidosBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.cardContenido1.isVisible = false
        binding.cardContenido2.isVisible = false
        binding.cardContenido3.isVisible = false
        binding.cardContenido4.isVisible = false

        getContenidos()
    }

    fun getContenidos(){


        if(prefs.getUserS1() == "MUY ALTA" || prefs.getUserS1() == "ALTA" || prefs.getUserS1() == "MODERADA"){
            binding.cardContenido1.setPadding(10)

            prefs.savePDFName("poo2.pdf")
            binding.cardContenido1.isVisible = true
        }

        if(prefs.getUserS2() == "MUY ALTA" || prefs.getUserS2() == "ALTA" || prefs.getUserS2() == "MODERADA"){
            binding.cardContenido2.setPadding(10)
            binding.cardContenido2.isVisible = true
        }

        if(prefs.getUserS3() == "MUY ALTA" || prefs.getUserS3() == "ALTA" || prefs.getUserS3() == "MODERADA"){
            binding.cardContenido3.setPadding(10)
                    prefs.savePDFName("poo1.pdf")
            binding.cardContenido3.isVisible = true
        }

        if(prefs.getUserS4() == "MUY ALTA" || prefs.getUserS4() == "ALTA" || prefs.getUserS4() == "MODERADA"){
            binding.cardContenido4.setPadding(10)
            binding.cardContenido4.isVisible = true
        }
    }

    fun verPDF(view: View){
        startActivity(Intent(this, PDFActivity::class.java))
    }

    fun escucharAudio(view: View){
        startActivity(Intent(this, AudioActivity::class.java))
    }
    fun seleccionarContenido(view: View) {
        startActivity(Intent(this, MostrarContenido::class.java))
    }
}