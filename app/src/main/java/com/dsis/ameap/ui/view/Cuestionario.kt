package com.dsis.ameap.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.dsis.ameap.R
import com.dsis.ameap.databinding.ActivityCuestionarioBinding
import com.dsis.ameap.databinding.ActivityMateriasBinding
import com.dsis.ameap.ui.visible

class Cuestionario : AppCompatActivity() {

    lateinit var binding:ActivityCuestionarioBinding
    private var mQuestionsList: ArrayList<Questions>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCuestionarioBinding.inflate(layoutInflater)

        setContentView(binding.root)
    }

    fun terminarCuestionario(view: View) {
        startActivity(Intent(this, CuestionarioEA::class.java))
    }
}