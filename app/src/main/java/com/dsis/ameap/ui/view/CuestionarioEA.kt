package com.dsis.ameap.ui.view

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.dsis.ameap.R
import com.dsis.ameap.databinding.ActivityCuestionarioBinding
import com.dsis.ameap.databinding.ActivityCuestionarioEaBinding
import com.dsis.ameap.repository.ApiRepository
import com.dsis.ameap.responses.UserStyle
import com.dsis.ameap.ui.viewmodel.Session
import com.dsis.ameap.ui.viewmodel.Session.Companion.prefs
import com.dsis.ameap.ui.visible
import com.google.gson.annotations.SerializedName

class CuestionarioEA : AppCompatActivity(), View.OnClickListener {

    val ESTILO_PRIORIDAD = arrayOf ("MUY BAJA", "BAJA", "MODERADA", "ALTA", "MUY ALTA")
    var mensaje = "A continuación se muestra cuales son los resultados del cuestionario:\n";
    private var mCurrentPosition: Int = 1
    private var mQuestionsList: ArrayList<Questions>? = null
    private var mSelectedOptionPosition: Int = 0
    private var mCorrectAnswers: Int = 0
    private val indxActivo =
        arrayOf(3, 5, 7, 9, 13, 20, 26, 27, 35, 37, 41, 43, 46, 48, 51, 61, 67, 74, 75, 77)
    private val indxReflexivo =
        arrayOf(10, 16, 18, 19, 28, 31, 32, 34, 36, 39, 42, 44, 49, 55, 58, 63, 65, 69, 70, 79)
    private val indxTeorico =
        arrayOf(2, 4, 6, 11, 15, 17, 21, 23, 25, 29, 33, 45, 50, 54, 60, 64, 66, 71, 78, 80)
    private val indxPragmatico =
        arrayOf(1, 8, 12, 14, 22, 24, 30, 38, 40, 47, 52, 53, 56, 57, 59, 62, 68, 72, 73, 76)
    public var activo = 0
    public var reflexivo = 0
    public var teorico = 0
    public var pragmatico = 0

    lateinit var binding: ActivityCuestionarioEaBinding
    lateinit var builder: AlertDialog.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCuestionarioEaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        builder = AlertDialog.Builder(this)

        var btn: Button? = null
        btn = findViewById<Button>(R.id.button8) as Button
        mQuestionsList = Constant.getQuestions()
        setQuestion()
        btn.setOnClickListener(this)
    }

    private fun setQuestion() {
        val question = mQuestionsList!!.get(mCurrentPosition - 1)
        var txtView: TextView? = null
        var tv_questions: TextView? = null
        var progressBar: ProgressBar? = null
        var btn: Button? = null
        btn = findViewById<Button>(R.id.button8) as Button

        if (mCurrentPosition == mQuestionsList!!.size) {
            btn.text = "Terminar"
        } else {
            btn.text = "Siguiente"
        }

        progressBar = findViewById<ProgressBar>(R.id.progressBar) as ProgressBar
        txtView = findViewById<TextView>(R.id.countLabel) as TextView
        tv_questions = findViewById<TextView>(R.id.tv_questions) as TextView

        progressBar.progress = mCurrentPosition
        txtView.text = "$mCurrentPosition" + "/" + progressBar.max
        tv_questions.text = question!!.question
    }


    override fun onClick(v: View?) {
        println("NÚMERO DE PREGUNTA: " + mCurrentPosition)

        if (!(binding.radioSi.isChecked) && !(binding.radioNo.isChecked)) {
            builder.setTitle("Cuestionario")
                .setMessage("Para avanzar a la siguiente pregunta tiene que elegir alguna opción.")
                .setCancelable(true)
                .setPositiveButton("Ok") { dialogInterface, it ->
                    dialogInterface.cancel()
                }
                .show()
        } else {
            if (binding.radioSi.isChecked) {
//                println("RESPUESTA SI")
                if (indxActivo.contains(mCurrentPosition)) {
                    println("SUMA UNO DE ACTIVO")
                    activo++;
                } else if (indxReflexivo.contains(mCurrentPosition)) {
                    println("SUMA UNO DE REFLEXIVO")
                    reflexivo++;
                } else if (indxTeorico.contains(mCurrentPosition)) {
                    println("SUMA UNO DE TEORICO")
                    teorico++;
                } else if (indxPragmatico.contains(mCurrentPosition)) {
                    println("SUMA UNO DE PRAGMATICO")
                    pragmatico++;
                }

            } else if (binding.radioNo.isChecked) {
                println("RESPUESTA NO")
            }

            var btn = findViewById<Button>(R.id.button8) as Button
            when (v?.id) {
                R.id.button8 -> {
                    binding.progressBar.visible(true)
                    if (mSelectedOptionPosition == 0) {
                        mCurrentPosition++
                        when {
                            mCurrentPosition <= mQuestionsList!!.size -> {
                                setQuestion()
                            }
                            else -> {
                                validarEstilosAprendizaje()
                                builder.setTitle("Cuestionario")
                                    .setMessage(mensaje)
                                    .setCancelable(true)
                                    .setPositiveButton("Ok") { dialogInterface, it ->
                                        dialogInterface.cancel()
                                        val intent = Intent(this, MenuPrincipal::class.java)
                                        startActivity(intent)
                                    }
                                    .show()

                            }
                        }
                    } else {

                        if (mCurrentPosition == mQuestionsList!!.size) {
                            btn.text = "Terminar"
                        } else {
                            btn.text = "Siguiente"
                        }
                        mSelectedOptionPosition = 0
                    }
                }
            }

        }
        binding.radioGroup.clearCheck()

    }

    private fun validarEstilosAprendizaje() {
        saveUserActivo()
        saveUserReflexivo()
        saveUserTeorico()
        saveUserPragmatico()
    }

    private fun saveUserPragmatico() {
        println("PUNTOS DE PRAGMÁTICO "+pragmatico)
        var priority = ""

        if(pragmatico <= 8){
            priority = ESTILO_PRIORIDAD[0]
        }else if((pragmatico >= 9) && (pragmatico <= 10)){
            priority = ESTILO_PRIORIDAD[1]
        }else if((pragmatico >= 11) && (pragmatico <= 13)){
            priority = ESTILO_PRIORIDAD[2]
        }else if((pragmatico >= 14) && (pragmatico <= 15)){
            priority = ESTILO_PRIORIDAD[3]
        }else if((pragmatico >= 16) && (pragmatico <= 20)){
            priority = ESTILO_PRIORIDAD[4]
        }

        println(priority)
        prefs.saveUserS4(priority)
        mensaje += "Pragmático: "+priority
        val apiRepository = ApiRepository()

        val userStyleResponse = UserStyle(
            id = 0,
            prioridad = priority,
            usuario_id = prefs.getUserId(),
            estilo_id = 4,
            created_at = "",
            updated_at = ""
        )

        apiRepository.postUserStyle(userStyleResponse){
            println(it)
        }
    }

    private fun saveUserTeorico() {
        println("PUNTOS DE TEÓRICO "+teorico)
        var priority = ""

        if(teorico <= 6){
            priority = ESTILO_PRIORIDAD[0]
        }else if((teorico >= 7) && (teorico <= 9)){
            priority = ESTILO_PRIORIDAD[1]
        }else if((teorico >= 10) && (teorico <= 13)){
            priority = ESTILO_PRIORIDAD[2]
        }else if((teorico >= 14) && (teorico <= 15)){
            priority = ESTILO_PRIORIDAD[3]
        }else if((activo >= 16) && (activo <= 20)){
            priority = ESTILO_PRIORIDAD[4]
        }

        println(priority)
        prefs.saveUserS3(priority)
        mensaje += "Teórico: "+priority+"\n"
        val apiRepository = ApiRepository()

        val userStyleResponse = UserStyle(
            id = 0,
            prioridad = priority,
            usuario_id = prefs.getUserId(),
            estilo_id = 3,
            created_at = "",
            updated_at = ""
        )

        apiRepository.postUserStyle(userStyleResponse){
            println(it)
        }
    }

    private fun saveUserReflexivo() {
        println("PUNTOS DE REFLEXIVO "+reflexivo)
        var priority = ""

        if(reflexivo <= 10){
            priority = ESTILO_PRIORIDAD[0]
        }else if((reflexivo >= 11) && (reflexivo <= 13)){
            priority = ESTILO_PRIORIDAD[1]
        }else if((reflexivo >= 14) && (activo <= 17)){
            priority = ESTILO_PRIORIDAD[2]
        }else if((reflexivo >= 18) && (reflexivo <= 19)){
            priority = ESTILO_PRIORIDAD[3]
        }else if((reflexivo == 20)){
            priority = ESTILO_PRIORIDAD[4]
        }

        println(priority)
        prefs.saveUserS2(priority)
        mensaje += "Reflexivo: "+priority+"\n"

        val apiRepository = ApiRepository()

        val userStyleResponse = UserStyle(
            id = 0,
            prioridad = priority,
            usuario_id = prefs.getUserId(),
            estilo_id = 2,
            created_at = "",
            updated_at = ""
        )

        apiRepository.postUserStyle(userStyleResponse){
            println(it)
        }
    }

    private fun saveUserActivo() {
        println("PUNTOS DE ACTIVO "+activo)
        var priority = ""

        if(activo <= 6){
            priority = ESTILO_PRIORIDAD[0]
        }else if((activo >= 7) && (activo <= 8)){
            priority = ESTILO_PRIORIDAD[1]
        }else if((activo >= 9) && (activo <= 12)){
            priority = ESTILO_PRIORIDAD[2]
        }else if((activo >= 13) && (activo <= 14)){
            priority = ESTILO_PRIORIDAD[3]
        }else if((activo >= 15) && (activo <= 20)){
            priority = ESTILO_PRIORIDAD[4]
        }

        println(priority)
        prefs.saveUserS1(priority)

        mensaje += "Activo: "+priority+"\n"

        val apiRepository = ApiRepository()

        val userStyleResponse = UserStyle(
            id = 0,
            prioridad = priority,
            usuario_id = prefs.getUserId(),
            estilo_id = 1,
            created_at = "",
            updated_at = ""
        )

        apiRepository.postUserStyle(userStyleResponse){
            println(it)
        }
    }

}