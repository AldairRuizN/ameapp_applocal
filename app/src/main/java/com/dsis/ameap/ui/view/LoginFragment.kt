package com.dsis.ameap.ui.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.dsis.ameap.data.network.ApiService
import com.dsis.ameap.data.network.AuthApi
import com.dsis.ameap.databinding.FragmentLoginBinding
import com.dsis.ameap.repository.ApiRepository
import com.dsis.ameap.repository.AuthRepository
import com.dsis.ameap.responses.UserLResponse
import com.dsis.ameap.ui.base.BaseFragment
import com.dsis.ameap.ui.startNewActivity
import com.dsis.ameap.ui.viewmodel.AuthViewModel
import com.dsis.ameap.ui.viewmodel.Session.Companion.prefs
import com.dsis.ameap.ui.visible
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

class LoginFragment : BaseFragment<AuthViewModel, FragmentLoginBinding, AuthRepository>() {

    lateinit var builder: AlertDialog.Builder

    @Suppress("DEPRECATION")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        builder = AlertDialog.Builder(requireContext())
        binding.progressbar.visible(false)

        if((prefs.getUserId() != 0) ){
            checkUserCuestionario()
        }

//        viewModel.loginResponse.observe(viewLifecycleOwner, Observer {
//            binding.progressbar.visible(false)
//            when(it){
//                is  Resource.Success -> {
//                    Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_LONG).show()
//                    lifecycleScope.launch {
//                        userPreferences.saveAuthToken(it.value.access_token)
//                        requireActivity().startNewActivity(Cuestionario::class.java)
//                    }
//                }
//                is Resource.Failure -> {
//                    Toast.makeText(requireContext(), "Login fallido", Toast.LENGTH_SHORT).show()
//                }
//            }
//        })

        binding.btnLogin.setOnClickListener {
            binding.progressbar.visible(true)
            val email = binding.txtEmail.text.toString().trim()
            val password = binding.txtPassword.text.toString().trim()

//            val url = "http://192.168.0.153:8000/api/users/validar"
//
//            val bodyParam = FormBody.Builder().add("email","prueba@gmail.com").add("password","123").build()
//            println("BODY")
//            println(bodyParam)
//
//            val api = OkHttpClient()
//
//            val request = Request.Builder().addHeader("Content-Type","application/json")
//                .url(url)
//                .post(bodyParam)
//                .build()
//
//            api.newCall(request).enqueue(object : Callback{
//                override fun onFailure(call: Call, e: IOException) {
//                    binding.progressbar.visible(false)
//                    e.printStackTrace()
//                }
//
//                override fun onResponse(call: Call, response: Response) {
//                    response.use {
//                        if(response.isSuccessful){
//                            binding.progressbar.visible(false)
//                            var body = response?.body?.string()
//                            println(body)
//                        }else{
//                            binding.progressbar.visible(false)
//                            println("ALGO SALIO MAL")
//                        }
//                    }
//                }
//            })

            val apiRepository = ApiRepository()

            val userl = UserLResponse(
                id = 0,
                name = "",
                email = email,
                password = password,
                status = false,
                mensaje = ""
            )

            apiRepository.getUserValidate(userl){
                if (it?.id != null) {
                    println(it.status)
                    println(it.mensaje)
                    if(it.status == false){
                        builder.setTitle("Error Login")
                            .setMessage("Los datos son incorrectos. Vuelve a intenrarlo")
                            .setCancelable(true)
                            .setPositiveButton("Ok") { dialogInterface, it ->
                                dialogInterface.cancel()
                            }
                            .show()
                    }else{
                        prefs.saveAllData(it.id, it.name, it.email)
                        checkUserCuestionario()

                    }

                    binding.progressbar.visible(false)
                } else {
                    println("ERROR")
                }
            }
//            binding.progressbar.visible(true)
//            Handler().postDelayed(Runnable {
//                binding.progressbar.visible(false)
//                requireActivity().startNewActivity(Cuestionario::class.java)
//                //anything you want to start after 3s
//            }, 2000)
//            binding.progressbar.visible(true)
//            viewModel.login(email, password)
        }
    }

    private fun checkUserCuestionario() {
        var doCuestionario = true;

        val apiRepository = ApiRepository()
        val id = prefs.getUserId()

        println("ID "+id)
        apiRepository.getCountEstilosUser(id){
            println("COUNT "+it?.count)
            if(it?.count!! >= 4){
                doCuestionario = false
                prefs.saveUserCuestionario(doCuestionario)
                goToMenuPrincipal()
            }else{
                goToCuestionario()
            }
        }
    }

    fun goToCuestionario(){
        requireActivity().startNewActivity(Cuestionario::class.java)
    }

    fun goToMenuPrincipal(){
        requireActivity().startNewActivity(MenuPrincipal::class.java)
    }

    override fun getViewModel() = AuthViewModel::class.java

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentLoginBinding.inflate(inflater, container, false)

    override fun getFragmentRepository() =
        AuthRepository(remoteDataSource.buildApi(AuthApi::class.java))
}