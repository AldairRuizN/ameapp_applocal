package com.dsis.ameap.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.dsis.ameap.R
import com.dsis.ameap.databinding.ActivityMateriasBinding
import com.dsis.ameap.repository.ApiRepository
import com.dsis.ameap.responses.MateriaResponse
import com.dsis.ameap.ui.viewmodel.Session.Companion.prefs

class Materias : AppCompatActivity() {

    lateinit var binding: ActivityMateriasBinding
    var listMaterias :  List<MateriaResponse>? = null
    var materiaId = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMateriasBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getMaterias()

        var items = listOf("Programación", "Matemáticas", "Español")
        val adapter = ArrayAdapter(this, R.layout.list_item, items)
        binding.dropdownMaterias.setAdapter(adapter)


        binding.dropdownMaterias.setOnItemClickListener { adapterView, view, i, l ->

            materiaId = i + 1;
        }
    }

    private fun getMaterias() {
        val apiRepository = ApiRepository()

        apiRepository.getMaterias {
            listMaterias = listOf(it) as List<MateriaResponse>??;

            println(listMaterias.toString())

        }
    }


    fun buscarContenidos(view: View) {
        println("MATERIA SELECCIONADA ID "+materiaId)
        prefs.saveMateriaId(materiaId)
        startActivity(Intent(this, Contenidos::class.java))
    }
}