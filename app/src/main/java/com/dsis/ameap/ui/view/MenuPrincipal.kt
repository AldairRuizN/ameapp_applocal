package com.dsis.ameap.ui.view

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.dsis.ameap.R

class MenuPrincipal : AppCompatActivity() {

    private lateinit var permissionLauncher: ActivityResultLauncher<Array<String>>
    private var isActivityRecognitionGranted = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_principal)

        permissionLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()){ permissions ->
            isActivityRecognitionGranted = permissions[Manifest.permission.ACTIVITY_RECOGNITION] ?: isActivityRecognitionGranted
        }

        requestPermission()
    }

    private fun requestPermission(){
        println("ENTRO")
        isActivityRecognitionGranted = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACTIVITY_RECOGNITION
        ) == PackageManager.PERMISSION_GRANTED

        val permissionRequest : MutableList<String> = ArrayList()

        println("boolean "+isActivityRecognitionGranted)
        if(!isActivityRecognitionGranted){
            permissionRequest.add(Manifest.permission.ACTIVITY_RECOGNITION)
        }

        if(permissionRequest.isNotEmpty()){

            permissionLauncher.launch(permissionRequest.toTypedArray())
        }
    }

    fun materias(view: View) {
        startActivity(Intent(this, Materias::class.java))
    }

    fun usuario(view: View) {}
}