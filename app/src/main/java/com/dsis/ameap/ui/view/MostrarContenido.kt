package com.dsis.ameap.ui.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import com.dsis.ameap.R
import com.dsis.ameap.ui.base.ActivityTransitionReceiver
import com.dsis.ameap.ui.base.ActivityTransitionUtil
import com.google.android.gms.common.internal.TelemetryLogging.getClient
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityRecognitionClient
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions

class MostrarContenido : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    lateinit var client: ActivityRecognitionClient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mostrar_contenido)

        client = ActivityRecognition.getClient(this)

        checkPermission()
        }

    fun checkPermission(){
        println("CHECAR PERMISOS")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
            && !ActivityTransitionUtil.hasActivityTransitionPermissions(this)
        ) {
            requestActivityTransitionPermision()
        } else {
            requestForUpdates()
        }

    }

    private fun requestActivityTransitionPermision() {
        EasyPermissions.requestPermissions(
            this,
            "SE NECESITAN PERMISOS",
            123,
            Manifest.permission.ACTIVITY_RECOGNITION
        )
    }

    @SuppressLint("MissingPermission")
    private fun requestForUpdates() {
        println("CONSULTAR ACTIVIDAD")
        val task = ActivityRecognition.getClient(this)
            .requestActivityTransitionUpdates(
                ActivityTransitionUtil.request,
                getPendingIntent())

        task.addOnSuccessListener {
            println(it)
        }

        task.addOnFailureListener { e: Exception ->
            println(e.toString())
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        requestForUpdates()
    }

    private fun getPendingIntent():PendingIntent{
        val intent = Intent(this, ActivityTransitionReceiver::class.java)
        return PendingIntent.getBroadcast(
            this,
            122,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this, perms)){
            AppSettingsDialog.Builder(this).build().show()
        }else{
            requestActivityTransitionPermision()
        }
    }
}