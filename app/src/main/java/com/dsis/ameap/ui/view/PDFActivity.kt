package com.dsis.ameap.ui.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContentProviderCompat.requireContext
import com.dsis.ameap.R
import com.dsis.ameap.ui.base.ActivityTransitionReceiver
import com.dsis.ameap.ui.base.ActivityTransitionUtil
import com.dsis.ameap.ui.startNewActivity
import com.dsis.ameap.ui.viewmodel.Session.Companion.prefs
import com.dsis.ameap.ui.visible
import com.github.barteksc.pdfviewer.PDFView
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityRecognitionClient
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.io.IOException

class PDFActivity : AppCompatActivity(),  EasyPermissions.PermissionCallbacks {

    lateinit var builder: AlertDialog.Builder
    lateinit var pdfview : PDFView
    lateinit var media : MediaPlayer
    lateinit var client: ActivityRecognitionClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdfactivity)
        builder = AlertDialog.Builder(this)


        pdfview = findViewById(R.id.pdfView)

        pdfview.fromAsset(prefs.getPDFName()).load()

        checkPermission()



    }

    @Throws(IOException::class)
    fun getFileFromAssets(context: Context, fileName: String): File = File(context.cacheDir, fileName)
        .also {
            if (!it.exists()) {
                it.outputStream().use { cache ->
                    context.assets.open(fileName).use { inputStream ->
                        inputStream.copyTo(cache)
                    }
                }
            }
        }



    fun checkPermission(){
        println("CHECAR PERMISOS")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
            && !ActivityTransitionUtil.hasActivityTransitionPermissions(this)
        ) {
            requestActivityTransitionPermision()
        } else {
            requestForUpdates()
        }

    }

    private fun requestActivityTransitionPermision() {
        EasyPermissions.requestPermissions(
            this,
            "SE NECESITAN PERMISOS",
            123,
            Manifest.permission.ACTIVITY_RECOGNITION
        )
    }

    @SuppressLint("MissingPermission")
    private fun requestForUpdates() {
        println("CONSULTAR ACTIVIDAD")
        val task = ActivityRecognition.getClient(this)
            .requestActivityTransitionUpdates(
                ActivityTransitionUtil.request,
                getPendingIntent())

        task.addOnSuccessListener {
            println(it.toString())
            builder.setTitle("Cambio de contexto")
                    .setMessage("Hemos detectado estas caminando, te mostraremos un contenido que sea adecuado tu actividad")
                    .setCancelable(true)
                    .setPositiveButton("Ok") { dialogInterface, it ->
                        dialogInterface.cancel()
                        media = MediaPlayer.create(this, R.raw.audio2)
                        media.start()
//                    val filePath =  getFileFromAssets(this, "fileName.extension").absolutePath
//                    println(filePath)
//                    startActivity(Intent(this, AudioActivity::class.java))
                    }
                    .show()
//            Handler(Looper.getMainLooper()).postDelayed({
//                println("SJODJAOSDK")
//                builder.setTitle("Cambio de contexto")
//                    .setMessage("Hemos detectado estas caminando, te mostraremos un contenido que sea adecuado tu actividad")
//                    .setCancelable(true)
//                    .setPositiveButton("Ok") { dialogInterface, it ->
//                        dialogInterface.cancel()
//                        media = MediaPlayer.create(this, R.raw.audio2)
//                        media.start()
////                    val filePath =  getFileFromAssets(this, "fileName.extension").absolutePath
////                    println(filePath)
////                    startActivity(Intent(this, AudioActivity::class.java))
//                    }
//                    .show()
//            }, 6000)
        }

        task.addOnFailureListener { e: Exception ->
            println(e.toString())
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        requestForUpdates()
    }

    private fun getPendingIntent(): PendingIntent {
        val intent = Intent(this, ActivityTransitionReceiver::class.java)
        return PendingIntent.getBroadcast(
            this,
            122,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this, perms)){
            AppSettingsDialog.Builder(this).build().show()
        }else{
            requestActivityTransitionPermision()
        }
    }
}