package com.dsis.ameap.ui.view

data class Questions ( val id: Int,
    val question: String,
    val optionOne: String,
    val optionTwo: String,
    val correctAnswer: Int
)