package com.dsis.ameap.ui.viewmodel

import android.content.Context
import com.dsis.ameap.ui.view.Cuestionario

class Preferencias(val context: Context) {

    val SHARED_NAME = "AMEAAP";
    val SHARED_USERID = "id";
    val SHARED_NAMEUSER = "name";
    val SHARED_EMAIL = "email";
    val SHARED_CUESTIONARIO = "doCuestionario";
    val SHARED_MATERIAID = "materia_id";
    val storage = context.getSharedPreferences(SHARED_NAME, 0)

    fun saveAllData(id:Int, name:String, email:String){
        storage.edit().putInt(SHARED_USERID,id).apply()
        storage.edit().putString(SHARED_NAMEUSER,name).apply()
        storage.edit().putString(SHARED_EMAIL,email).apply()
    }

    fun savePDFName(pdf:String){
        storage.edit().putString("pdf",pdf).apply()
    }

    fun getPDFName():String{
        return storage.getString("pdf", "")!!
    }

    fun saveUserS1(id: String){
        storage.edit().putString("US1", id).apply()
    }

    fun saveUserS2(id: String){
        storage.edit().putString("US2", id).apply()
    }

    fun saveUserS3(id: String){
        storage.edit().putString("US3", id).apply()
    }

    fun saveUserS4(id: String){
        storage.edit().putString("US4", id).apply()
    }
    fun getUserS1():String{
        return storage.getString("US1", "")!!
    }
    fun getUserS2():String{
        return storage.getString("US2", "")!!
    }
    fun getUserS3():String{
        return storage.getString("US3", "")!!
    }
    fun getUserS4():String{
        return storage.getString("US4", "")!!
    }

    fun saveMateriaId(materia_id: Int){
        storage.edit().putInt(SHARED_MATERIAID, materia_id).apply()
    }

    fun saveUserCuestionario(doCuestionario: Boolean){
        storage.edit().putBoolean(SHARED_CUESTIONARIO, doCuestionario).apply()
    }

    fun getUserCuestionario():Boolean{
        return storage.getBoolean(SHARED_CUESTIONARIO, true)!!
    }

    fun getMateriaId():Int{
        return storage.getInt(SHARED_MATERIAID, 0)!!
    }

    fun getUserId():Int{
        return storage.getInt(SHARED_USERID, 0)!!
    }

    fun getUserName():String{
        return storage.getString(SHARED_NAMEUSER, "")!!
    }

    fun getUserEmail():String{
        return storage.getString(SHARED_EMAIL, "")!!
    }

    fun logout(){
        storage.edit().clear().apply()
    }

}