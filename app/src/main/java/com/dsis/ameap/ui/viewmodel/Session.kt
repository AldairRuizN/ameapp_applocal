package com.dsis.ameap.ui.viewmodel

import android.app.Application
import android.content.SharedPreferences

class Session : Application(){

    companion object{
        lateinit var prefs:Preferencias
    }

    override fun onCreate() {
        super.onCreate()
        prefs = Preferencias(applicationContext)
    }
}